<?php
/**
 * appSync unit tests for sync between clients and server
 * version: 0.1
 * date: 18/07/2014
 *
 */

include("appsync.php");
$conflictHandling = TIMESTAMPPRIORITY;

$server = new Server("server");
$client1 = new Client("client1", $server);

$client1->display();
$server->display();

echo "*** START Creating apples on client 1\n";
$client1->addObject("2014-05-10", "apples", "3");
echo "*** END   Creating apples on client 1\n";
$client1->display();

echo "*** START Do sync on client 1\n";
$client1->doSync();
echo "*** END   Do sync on client 1\n";
$client1->display();
$server->display();

echo "*** START Updating apples on client 1\n";
$client1->updateObject("2014-05-10", "5");
echo "*** END   Updating apples on client 1\n";
$client1->display();

sleep(2); //make sure timestamp_lastupdate on client and server is different

echo "*** START Updating apples on server\n";
$server->updateObject("2014-05-10", "7");
echo "*** END   Updating apples on server\n";
$server->display();

echo "*** START Do sync on client 1\n";
$client1->doSync();
echo "*** END   Do sync on client 1\n";
$client1->display();
$server->display();

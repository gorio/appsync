<?php
/**
 * appSync unit tests for sync between clients and server
 * version: 0.1
 * date: 18/07/2014
 *
 */

include("appsync.php");
$conflictHandling = TIMESTAMPPRIORITY;

/*
 * Test scenario's:
 * 1. sync from server to client: new objects and object updates
 * 2. sync from client to server: new objects and object updates
 * 3. sync from client to server to other client
 * 4. no unneeded syncing (e.g. client syncs update to server, and when client syncs again it receives its own update again, this should not occur)
 * 5. syncing of deleted objects (isdeleted=1)
 * 6. syncing with conflict handling: object is updated on client and on server and then syncing takes place
 * 7. syncing with primary key conflict: object with same PK is created both on client and on server and then syncing takes place
 * 8. syncing with primary key conflict: object with same PK is created client A and client B and then syncing takes place
 * 9. full sync, with locally created objects that are not synced yet
 *
 * Below is a random combination of various unit tests.
 * See separate PHP files for individual unit tests.
*/

$server = new Server("server");
$client1 = new Client("client1", $server);
$client2 = new Client("client2", $server);

$client1->display();
$server->display();

echo "*** START Creating apples on client 1\n";
$client1->addObject("2014-05-10", "apples", "3");
echo "*** END   Creating apples on client 1\n";
$client1->display();

echo "*** START Do sync on client 1\n";
$client1->doSync();
echo "*** END   Do sync on client 1\n";
$client1->display();
$server->display();

echo "*** START Sync again on client 1 (no data should be synced) \n";
$client1->doSync();
echo "*** END Sync again on client 1 (no data should be synced) \n";
$client1->display();
$server->display();

echo "*** START Sync on client 2\n";
$client2->doSync();
echo "*** END   Sync on client 2\n";
$client2->display();
$server->display();

echo "*** START Updating apples on client 1\n";
$client1->updateObject("2014-05-10", "5");
echo "*** END   Updating apples on client 1\n";
$client1->display();

echo "*** START Do sync on client 1\n";
$client1->doSync();
echo "*** END   Do sync on client 1\n";
$client1->display();
$server->display();

echo "*** START Sync on client 2\n";
$client2->doSync();
echo "*** END   Sync on client 2\n";
$client2->display();
$server->display();

echo "*** START Updating apples on client 2\n";
$client2->updateObject("2014-05-10", "7");
echo "*** END   Updating apples on client 2\n";
$client2->display();

echo "*** START Sync on client 2\n";
$client2->doSync();
echo "*** END   Sync on client 2\n";
$client2->display();
$server->display();

echo "*** START Do sync on client 1\n";
$client1->doSync();
echo "*** END   Do sync on client 1\n";
$client1->display();
$server->display();

echo "*** START Updating apples on server\n";
$server->updateObject("2014-05-10", "9");
echo "*** END   Updating apples on server\n";
$server->display();

echo "*** START Do sync on client 1\n";
$client1->doSync();
echo "*** END   Do sync on client 1\n";
$client1->display();
$server->display();

echo "*** START Sync on client 2\n";
$client2->doSync();
echo "*** END   Sync on client 2\n";
$client2->display();
$server->display();

echo "*** START Sync again on client 1 (no data should be synced) \n";
$client1->doSync();
echo "*** END   Sync again on client 1 (no data should be synced) \n";
$client1->display();
$server->display();

echo "*** START Sync again on client 2 (no data should be synced) \n";
$client2->doSync();
echo "*** END   Sync again on client 2 (no data should be synced) \n";
$client2->display();
$server->display();

echo "*** START Creating oranges on client 1\n";
$client1->addObject("2014-05-11", "oranges", "4");
echo "*** END   Creating oranges on client 1\n";
$client1->display();

echo "*** START Full sync on client 1 (all data should be synced) \n";
$client1->doFullSync();
echo "*** END   Full sync on client 1 (all data should be synced) \n";
$client1->display();
$server->display();

echo "*** START Sync again on client 1 (no data should be synced) \n";
$client1->doSync();
echo "*** END   Sync again on client 1 (no data should be synced) \n";
$client1->display();
$server->display();
